﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IniParser;
using IniParser.Model;

namespace HavenRosterTracker
{
    public static class SettingsManager
    {
        private static string SECTION_PLAYER_HIGHLIGHTING = "Player Hightlighting Settings";
        private static string KEY_NOT_READY_ILVL = "NotReadyILvl";
        private static string KEY_READY_ILVL = "IsReadyILvl";
        private static string KEY_CHECK_LEGENDARY = "CheckLegendary";

        private static string KEY_COLOR_NOT_READY = "NotReadyColor";
        private static string KEY_COLOR_ALMOST_READY = "AlmostReady";
        private static string KEY_COLOR_IS_READY = "IsReadyColor";
        private static string KEY_COLOR_LEGENDARY = "LegendaryColor";

        private static string FILE_CONFIG = "Configuration.ini";

        private static bool DoSettingsAlreadyExist()
        {
            bool result = true;

            if (Properties.Settings.Default.notReadyILvl != 0 ||
                Properties.Settings.Default.isReadyILvl != 0 ||
                Properties.Settings.Default.checkLegendary != false ||
                !string.IsNullOrWhiteSpace(Properties.Settings.Default.notReadyColor) ||
                !string.IsNullOrWhiteSpace(Properties.Settings.Default.almostReadyColor) ||
                !string.IsNullOrWhiteSpace(Properties.Settings.Default.isReadyColor) ||
                !string.IsNullOrWhiteSpace(Properties.Settings.Default.legendaryColor))
            {
                result = false;
            }

            return result;
        }

        public static void Init()
        {           
            if(!DoSettingsAlreadyExist())
            {
                if (File.Exists(FILE_CONFIG))
                {
                    FileIniDataParser parser = new FileIniDataParser();
                    IniData data = parser.ReadFile(FILE_CONFIG);

                    Properties.Settings.Default.notReadyILvl = uint.Parse(data[SECTION_PLAYER_HIGHLIGHTING][KEY_NOT_READY_ILVL]);
                    Properties.Settings.Default.isReadyILvl = uint.Parse(data[SECTION_PLAYER_HIGHLIGHTING][KEY_READY_ILVL]);
                    Properties.Settings.Default.checkLegendary = bool.Parse(data[SECTION_PLAYER_HIGHLIGHTING][KEY_CHECK_LEGENDARY]);

                    Properties.Settings.Default.notReadyColor = data[SECTION_PLAYER_HIGHLIGHTING][KEY_COLOR_NOT_READY];
                    Properties.Settings.Default.almostReadyColor = data[SECTION_PLAYER_HIGHLIGHTING][KEY_COLOR_ALMOST_READY];
                    Properties.Settings.Default.isReadyColor = data[SECTION_PLAYER_HIGHLIGHTING][KEY_COLOR_IS_READY];
                    Properties.Settings.Default.legendaryColor = data[SECTION_PLAYER_HIGHLIGHTING][KEY_COLOR_LEGENDARY];
                    Properties.Settings.Default.Save();
                }
            }           
        }

        public static void ExportSettingsToIni()
        {
            FileIniDataParser parser = new FileIniDataParser();
            IniData data = new IniData();

            data.Sections.AddSection(SECTION_PLAYER_HIGHLIGHTING);
            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_NOT_READY_ILVL, Properties.Settings.Default.notReadyILvl.ToString());
            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_READY_ILVL, Properties.Settings.Default.isReadyILvl.ToString());

            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_CHECK_LEGENDARY, Properties.Settings.Default.checkLegendary.ToString());

            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_COLOR_NOT_READY, Properties.Settings.Default.notReadyColor.ToString());
            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_COLOR_ALMOST_READY, Properties.Settings.Default.almostReadyColor.ToString());
            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_COLOR_IS_READY, Properties.Settings.Default.isReadyColor.ToString());
            data[SECTION_PLAYER_HIGHLIGHTING].AddKey(KEY_COLOR_LEGENDARY, Properties.Settings.Default.legendaryColor.ToString());

            parser.WriteFile(FILE_CONFIG, data);
        }
    }
}
