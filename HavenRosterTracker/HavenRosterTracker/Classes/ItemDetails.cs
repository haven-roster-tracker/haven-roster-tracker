﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.Classes
{
    public class ItemDetails
    {
        public string Slot { get; set; }
        public string ItemName { get; set; }
        public int ILevel { get; set; }
        public string Quality { get; set; }
    }
}
