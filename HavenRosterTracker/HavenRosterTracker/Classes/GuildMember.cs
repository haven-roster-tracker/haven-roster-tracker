﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.Classes
{
    public class GuildMember
    {
        public string PlayerName { get; set; }
        public string CharacterName { get; set; }
        public int CharacterLevel { get; set; }
        public string CharacterRace { get; set; }
        public string CharacterClass { get; set; }
        public string CharacterRealm { get; set; }
        public int GuildRank { get; set; }
        public CharacterEquipment CharacterEquipment { get; set; }
        public float AverageItemLevel
        {
            get
            {
                if (this.CharacterEquipment != null)
                    return this.CharacterEquipment.GetAverageILevel();
                else
                    return 0;
            }
        }

        public bool HasLegendary 
        {
            get
            {
                return this.CharacterEquipment.hasLegendary();
            }

            set
            {
                //Do Nothing.
            }
        }

        public string HasLegendaryColor
        {
            get
            {
                if(Properties.Settings.Default.checkLegendary)
                {
                    return Properties.Settings.Default.legendaryColor;
                }
                else
                {
                    return ReadyStatusColor;
                }                
            }

            set
            {
                //Do Nothing.
            }
        }

        public string ReadyStatusColor
        {
            get
            {
                string colorValue = "White";
                if(AverageItemLevel != 0)
                {
                    if (AverageItemLevel < Properties.Settings.Default.notReadyILvl)
                    {
                        colorValue = Properties.Settings.Default.notReadyColor;
                    }
                    else if (AverageItemLevel >= Properties.Settings.Default.notReadyILvl &&
                        AverageItemLevel < Properties.Settings.Default.isReadyILvl)
                    {
                        colorValue = Properties.Settings.Default.almostReadyColor;
                    }
                    else if (AverageItemLevel > Properties.Settings.Default.isReadyILvl)
                    {
                        colorValue = Properties.Settings.Default.isReadyColor;
                    }
                }                

                return colorValue;
            }
        }
    }
}
