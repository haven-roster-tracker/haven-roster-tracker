﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.Classes
{
    public class CharacterEquipment
    {
        private string QUALITY_LEGENDARY = "LEGENDARY";

        Dictionary<string, ItemDetails> _EquippedItems;
        bool _hasLegendary = false;


        public CharacterEquipment()
        {
            _EquippedItems = new Dictionary<string, ItemDetails>();
        }

        public void AddEquipment(ItemDetails item)
        {
            if(!_EquippedItems.ContainsKey(item.Slot))
            {
                _EquippedItems.Add(item.Slot, item);
            }
            else
            {
                _EquippedItems[item.Slot] = item;
            }

            if(item.Quality.ToUpper().Equals(QUALITY_LEGENDARY))
            {
                _hasLegendary = true;
            }
        }

        public ItemDetails GetSlotInfo(string slot)
        {
            if (_EquippedItems.ContainsKey(slot))
                return _EquippedItems[slot];
            else
                return null;
        }

        public Dictionary<string, ItemDetails> GetAllEquippedItems()
        {
            return _EquippedItems;
        }

        public float GetAverageILevel()
        {
            float totalILevel = 0;
            int numItemsEquipped = 0;
            float result = 0;

            foreach(KeyValuePair<string, ItemDetails> item in _EquippedItems)
            {
                if(!item.Key.Equals("Shirt") && !item.Key.Equals("Tabard"))
                {
                    numItemsEquipped++;
                    totalILevel += item.Value.ILevel;
                }                    
            }

            //If a Character Only has a Main hand, Blizzard counts it twice
            if(!_EquippedItems.ContainsKey("Off Hand") && _EquippedItems.ContainsKey("Main Hand"))
            {
                numItemsEquipped++;
                totalILevel += _EquippedItems["Main Hand"].ILevel;
            }

            if (numItemsEquipped > 0)
            {
                result = totalILevel / numItemsEquipped;
            }
            else
            {
                Console.WriteLine("0 Items Grabbed????");
            }

            return result;
        }

        public bool hasLegendary()
        {
            return _hasLegendary;
        }
    }
}
