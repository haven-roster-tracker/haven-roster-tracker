﻿using HavenRosterTracker.Classes;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HavenRosterTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string STATUS_FORMAT_PROCESSING = "Processing [{0}/{1}]: {2} - {3}";

        private static string STATUS_COMPLETE = "Roster Refresh Complete";

        ObservableCollection<GuildMember> _roster;

        public MainWindow()
        {
            InitializeComponent();
            SettingsManager.Init();
            BlizzardAPIManager.Init();
            _roster = BlizzardAPIManager.GetGuildRoster();
            this.dgRosterStatus.ItemsSource = _roster;
            this.Title = "Haven Roster Tracker - v0.3";
        }

        private void GetRosterEquipment(ObservableCollection<GuildMember> roster)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => {
                windowProgress.Minimum = 1;
                windowProgress.Maximum = roster.Count;
            }));

            foreach (GuildMember member in roster)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => { 
                    lblStatus.Text = string.Format(STATUS_FORMAT_PROCESSING, roster.IndexOf(member) + 1, roster.Count, member.CharacterName, member.CharacterRealm);
                    windowProgress.Value = roster.IndexOf(member);
                }));
                
                member.CharacterEquipment = BlizzardAPIManager.GetCharacterEquipment(member.CharacterName, member.CharacterRealm);
            }
        }

        private async Task refreshRoster()
        {
            await Task.Run(() =>
            {
                GetRosterEquipment(_roster);
            });

            lblStatus.Text = STATUS_COMPLETE;
            refreshDatagrid();
        }

        private void refreshDatagrid()
        {
            this.dgRosterStatus.ItemsSource = null;
            this.dgRosterStatus.ItemsSource = _roster;
        }

        private async void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            await refreshRoster();

        }

        private void btnExportCsv_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CSV Files | *.csv";
            if (sfd.ShowDialog() == true)
            {
                BlizzardAPIManager.PrintGuildRoster(_roster, sfd.FileName);
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {            
            await refreshRoster();
            refreshDatagrid();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Windows.ConfigWindow cfg = new Windows.ConfigWindow();
            if(cfg.ShowDialog() == true)
            {
                SettingsManager.ExportSettingsToIni();
                refreshDatagrid();
            }
        }
    }
}
