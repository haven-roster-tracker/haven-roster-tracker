﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HavenRosterTracker;

namespace HavenRosterTracker.Windows
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        public ConfigWindow()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.notReadyILvl = (uint)notReadyUpDown.Value;
            Properties.Settings.Default.isReadyILvl = (uint)readyUpDown.Value;
            Properties.Settings.Default.checkLegendary = legendaryChkbox.IsChecked.Value;

            Properties.Settings.Default.notReadyColor = NotReadyColor.SelectedColor.Value.ToString();
            Properties.Settings.Default.almostReadyColor = AlmostReadyColor.SelectedColor.Value.ToString();
            Properties.Settings.Default.isReadyColor = IsReadyColor.SelectedColor.Value.ToString();
            Properties.Settings.Default.legendaryColor = LegendaryColor.SelectedColor.Value.ToString();
            Properties.Settings.Default.Save();

            this.DialogResult = true;            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            notReadyUpDown.Value = Properties.Settings.Default.notReadyILvl;
            readyUpDown.Value = Properties.Settings.Default.isReadyILvl;
            legendaryChkbox.IsChecked = Properties.Settings.Default.checkLegendary;

            if(!string.IsNullOrWhiteSpace(Properties.Settings.Default.notReadyColor))
            {
                NotReadyColor.SelectedColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(Properties.Settings.Default.notReadyColor);
            }

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.almostReadyColor))
            {
                AlmostReadyColor.SelectedColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(Properties.Settings.Default.almostReadyColor);
            }

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.isReadyColor))
            {
                IsReadyColor.SelectedColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(Properties.Settings.Default.isReadyColor);
            }

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.legendaryColor))
            {
                LegendaryColor.SelectedColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(Properties.Settings.Default.legendaryColor);
            }

        }
    }
}
