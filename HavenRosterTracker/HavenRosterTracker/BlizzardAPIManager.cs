﻿using HavenRosterTracker.Classes;
using HavenRosterTracker.JSONObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker
{
    public static class BlizzardAPIManager
    {
        private const string SECRET_ID = "56291907fc404e09b973702d3f2350ae";
        private const string CLIENT_SECRET = "v9BA19hCN2WQyrPpKIgcvfWfG1fJaby9";

        private const string BASE_URL = @"https://us.api.blizzard.com";

        private const string LOCALE = "en_us";
        private const string REGION = "us";

        /// <summary>
        /// 0 - Region
        /// </summary>
        private const string FORMAT_NAMESPACE = "profile-{0}";

        /// <summary>
        /// 0 - Guild Realm
        /// 1 - Guild Name
        /// 2 - locale
        /// 3 - access token
        /// 4 - namespace
        /// </summary>
        private const string ENDPOINT_GUILD_ROSTER = @"/data/wow/guild/{0}/{1}/roster?locale={2}&access_token={3}&namespace={4}";

        /// <summary>
        /// 0 - Realm
        /// 1 - Name
        /// 2 - locale
        /// 3 - access token
        /// 4 - namespace
        /// </summary>
        private const string ENDPOINT_CHARACTER_EQUIPMENT = @"/profile/wow/character/{0}/{1}/equipment?locale={2}&access_token={3}&namespace={4}";

        private static string _ApiNamespace;
        private static string _ApiAccessToken;
        private static string _GuildName;
        private static string _GuildRealm;

      

        public static void Init()
        {
            GetAPIToken();
            _ApiNamespace = string.Format(FORMAT_NAMESPACE, REGION);
            _GuildName = "haven";
            _GuildRealm = "rivendare";
        }

        private static void GetAPIToken()
        {
            string base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(SECRET_ID+":"+CLIENT_SECRET));
            RestClient client = new RestClient("https://us.battle.net/oauth/token");
            client.Timeout = -1;
            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", "Basic "+ base64authorization);
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("scope", "wow.profile");            
            IRestResponse response = client.Execute(request);

            AuthToken token = JsonConvert.DeserializeObject<AuthToken>(response.Content);
            _ApiAccessToken = token.access_token;
        }

        private static IRestResponse ExecuteRestEndpoint(string endpoint, Method method)
        {
            RestClient client = new RestClient(BASE_URL + endpoint);
            client.Timeout = -1;
            RestRequest request = new RestRequest(method);
            return client.Execute(request);
        }

        private static List<GuildMember> GenerateGuildRoster(GuildRosterJSON roster)
        {
            List<GuildMember> result = new List<GuildMember>();

            foreach(GuildRosterEntryJSON guildRosterEntry in roster.members)
            {
                if (guildRosterEntry.rank == 1 || guildRosterEntry.rank == 3)
                {
                    GuildMember tempMember = new GuildMember();

                    tempMember.PlayerName = "N/A";
                    tempMember.CharacterName = guildRosterEntry.character.name;
                    tempMember.CharacterLevel = guildRosterEntry.character.level;
                    tempMember.CharacterClass = guildRosterEntry.character.playable_class.GetIDString();
                    tempMember.CharacterRace = guildRosterEntry.character.playable_race.GetIDString();
                    tempMember.GuildRank = guildRosterEntry.rank;
                    tempMember.CharacterRealm = guildRosterEntry.character.realm.slug;

                    result.Add(tempMember);
                }                    
            }

            return result;
        }

        public static void PrintGuildRoster(ObservableCollection<GuildMember> guildMembers, string path)
        {
            List<string> fileRows = new List<string>();
            fileRows.Add("Character Name,Character Level,Average Item Level,Character Class,Character Race,Guild Rank");
            foreach (GuildMember member in guildMembers)
            {
                fileRows.Add(string.Format("{0},{1},{2},{3},{4},{5}", member.CharacterName, member.CharacterLevel, member.CharacterEquipment.GetAverageILevel(), member.CharacterClass, member.CharacterRace, member.GuildRank));
            }

            File.WriteAllLines(path, fileRows);
        }

        public static CharacterEquipment GetCharacterEquipment(string name, string realm)
        {
            CharacterEquipment result = new CharacterEquipment();
            string endpoint = string.Format(ENDPOINT_CHARACTER_EQUIPMENT, realm.ToLower(), name.ToLower(), LOCALE, _ApiAccessToken, _ApiNamespace);
            IRestResponse response = ExecuteRestEndpoint(endpoint, Method.GET);

            if(response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                CharacterEquipmentJSON equipment = JsonConvert.DeserializeObject<CharacterEquipmentJSON>(response.Content);

                foreach(ItemJSON item in equipment.equipped_items)
                {
                    ItemDetails itemDetails = new ItemDetails();
                    itemDetails.ItemName = item.name;
                    itemDetails.ILevel = item.level.value;
                    itemDetails.Slot = item.slot.name;
                    itemDetails.Quality = item.quality.type;

                    result.AddEquipment(itemDetails);
                }
            }
            result.GetAverageILevel();

            return result;
        }
        

        public static ObservableCollection<GuildMember> GetGuildRoster()
        {
            string endpoint = string.Format(ENDPOINT_GUILD_ROSTER, _GuildRealm, _GuildName, LOCALE, _ApiAccessToken, _ApiNamespace);

            IRestResponse response = ExecuteRestEndpoint(endpoint, Method.GET);

            GuildRosterJSON roster = JsonConvert.DeserializeObject<GuildRosterJSON>(response.Content);

            return new ObservableCollection<GuildMember>(GenerateGuildRoster(roster));

            //Pull character data to get everything else easier? We probably just need name/guild rank from guild roster.
        }
    }
}
