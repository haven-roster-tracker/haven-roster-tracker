﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class WoWClassJSON
    {
        private string[] _ClassTypes = new string[] {"", "Warrior","Paladin", "Hunter",
                                                    "Rogue", "Priest", "Death Knight",
                                                    "Shaman", "Mage", "Warlock",
                                                    "Monk", "Druid", "Demon Hunter" };

        public int id { get; set; }

        public string GetIDString()
        {
            return _ClassTypes[this.id];
        }
    }
}
