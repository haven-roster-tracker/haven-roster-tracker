﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class CharacterEquipmentJSON
    {
        public List<ItemJSON> equipped_items { get; set; }
    }
}
