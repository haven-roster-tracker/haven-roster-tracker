﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class WoWRaceJSON
    {
        private Dictionary<int, string> _RaceTypes = new Dictionary<int, string>
        {
            {1,"Human"}, {2,"Orc"}, {3,"Dwarf"}, {4,"Night Elf"},
            {5,"Undead"}, {6,"Tauren"}, {7,"Gnome"}, {8,"Troll"},
            {9,"Goblin"}, {10,"Blood Elf"}, {11,"Dranei"}, {22,"Worgen"},
            {24,"Pandaren"}, {25,"Pandaren"},  {26,"Pandaren"}, {27,"Nightborne"},
            {28,"Highmountain Tauren"}, {29,"Void Elf"}, {30,"Lightforged Draenei"},
            {31,"Zandalari Troll"}, {32,"Kul Tiran"}, {34,"Dark Iron Dwarf"},
            {35,"Vulpera"}, {36,"Mag'Har Orc"}, {37,"Mechagnome"}
        };
        public int id { get; set; }
        public string GetIDString()
        {
            return _RaceTypes[this.id];
        }
    }
}
