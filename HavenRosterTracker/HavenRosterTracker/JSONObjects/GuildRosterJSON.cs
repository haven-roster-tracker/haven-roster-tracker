﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class GuildRosterJSON
    {
        public List<GuildRosterEntryJSON> members {get; set;}
    }
}
