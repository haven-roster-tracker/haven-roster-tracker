﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class EquippedItemJSON
    {
        public ItemJSON item { get; set; }
    }
}
