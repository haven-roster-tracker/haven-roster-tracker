﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class ItemJSON
    {
        public TypeNameJSON slot { get; set; }
        public ILevelJSON level { get; set; }
        public TypeNameJSON quality { get; set; }
        public string name { get; set; }
    }
}
