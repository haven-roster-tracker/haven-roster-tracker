﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class GuildRosterEntryJSON
    {
        public GuildCharacterJSON character { get; set; }
        public int rank { get; set; }
    }
}
