﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class GuildCharacterJSON
    {
        public string name { get; set; }
        public int level { get; set; }
        public WoWClassJSON playable_class { get; set; }
        public WoWRaceJSON playable_race { get; set; }
        public WoWRealmJSON realm { get; set; }
    }
}
