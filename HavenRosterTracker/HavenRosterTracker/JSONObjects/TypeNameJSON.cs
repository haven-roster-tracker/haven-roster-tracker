﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HavenRosterTracker.JSONObjects
{
    public class TypeNameJSON
    {
        public string type { get; set; }
        public string name { get; set; }
    }
}
